# Pěstování 2022

## Květináče (Tomáš)

 - [X] Začátkem [[duben]] jsem do skleníku do květináčů dal předpěstovat [[roketa]] (rostou), [[rericha]] (roste), [[salat]]
 - Update [[kveten]] - saláty nevyklíčily, zbytek jo, rostly docela bujně, úspěch, příště by to chtělo víc.

## Bylinková zahrádka
### Jednoletá část

#jana - někde sehnat majoránku, libeček. Nápad: Co vlastní bobkáč?

 - [x] [[brezen]]: [[mrkev]] (raná i pozdní)
 - [x] [[brezen]]  [[petrzel natova]] (na proužku u domu - nevyrostla), [[cibule]] na nať (zatím nevyrostla #TODO), 
 - [x] [[brezen]] [[spenat]], [[redkvicky]], [[roketa]] - vše všude kromě nejbližšího proužku u domu
 - [x] [[duben]]:  Kolem bylinkové zahrádky [[cibule]] a [[cesnek]] (jarní z Pelhřimova). Obojí rostě pěkně.
 - [x] [[duben]] 25 vysazena [[petrzel kaderava]] a [[petrzel kudrnka]]

## Skleník 

 - [X] [[brezen]]: Předpěstování [[brokolice]], [[kedluben]] a [[ruzickova kapusta]]; 
 - [x] v [[brezen]] do skleníku [[mrkev]] pro časnou sklizeň
 - [x] [[brezen]] (konec) na straně u silnice nejblíž k domu vysety [[papriky]] - tyhle nevzešly (konec dubna)
 - [x] [[brezen]] (konec) na straně u potoka [[spenat]]  - roste (půlka dubna), matador, [[roketa]], [[rericha]], [[kaderavek]], [[redkvicky]], [[cibule]] a [[mrkev]], nejblíž u domu jen [[mrkev]] a úplně nejblíž [[petrzel kaderava]]
 - [x] [[brezen]] (konec) na straně u Samka [[kostaloviny]] k předpěstování – [[kedlubny]], [[brokolice]], [[zeli]], [[mochyne peruanska]] a [[ruzickova kapusta]], pár [[celer]]ů
 - [x] [[duben]] 24:  Vysety [[okurky]] salatove a papriky ze semínek, u silnice v polovině směrem k baráku
 - [x] [[kveten]]: Sklizena většina věcí až na nějaké [[kostaloviny]]. Vysazeny 7x [[rajcata]] a 5x [[papriky]] (2x Kubovy) a  5x [[okurky]] (4x semínko - vzešly, 1x koupená),

### Review skleník

[[rajcata]] [[okurky]] se letos docela povedly, ale začaly rel. brzo plesnivět. [[kostaloviny]] sežraly [[housenky]] bělásků.



## Záhon před skleníkem

 - [x] [[duben]] 24. Na severní straně [[okurky]] ze semínek (stejné jako ve skleníku, salátovky) a [[kopr]]
 - [x] [[duben]] 25. vysazeny [[kedlubny]], [[zeli]], [[ruzickova kapusta]] a [[salat]].
 - [[kveten]] 5. - žerou nám to malý [[slimaci]], hlavně [[kedlubny]], máme cca 10 ks a některý už mají kriticky ožraný listí. Okem jsem je ale viděl hlavně na [[salat]]u, ale ten se zdá míň ožratej.  Zkusil jsem okolo nich dát na cca 15 cm vysoký válce z nařezanýho okapu, tak uvidíme. 
 - Update [[kveten]] 23. - nějak se to z větší části zachránilo - vyrostlo to dřív než to [[slimaci]] sežrali.
 - [x] [[kveten]] 20:  vysazeny [[kostaloviny]] - něco předpěstované ze skleníku, něco z Domašína. Na jižní straně vysázeny 4x [[okurky]] nakladačky z Vráže. Vypadají docela okusovaně, [[slimaci]]? A navíc k tomu vzešly nějaké vyseté [[okurky]], a [[kopr]]. Celé jsme to zamulčovali dvěma pytli štěpky. A ještě k tomu vysazena koupená [[okurky]].

## Záhon s listím
 - [x] [[kveten]] 4. vyset [[hrasek]]
 - Další plán na letošek není

## Záhon u kompostu (po kopřivách)

 - [x] [[duben]]:20. vysety rane [[brambory]], [[dyne]], [[cukety]], [[patizon]]. Vše ze semínek/hlíz, jen 3 cukety vysety


## Superzáhon

 - [x] [[kveten]] 4: [[fazole]] - blíž k domu velké fazole, dál od domu malé #TODO zjistit odrůdy
 - [x] [[kveten]] 20. přesadí se sem košťáloviny ze skleníku, vypadají že prosperují, mezi nimi rostou fazole.


## Dřevěný záhon

 - [x] [[duben]] 21 půlka blíž u silnice vyseta [[mrkev]], [[porek]], [[kedlubny]] červené
 - [x] [[kveten]] 7. vyset [[mangold]], [[cekanka]], [[cervena repa]]. 
 - [x] [[kveten]] 23. vysazeny 2x [[rajcata]], 5x [[papriky]] (2x Kubovy)
 -  [[kveten]] 23. Vypadá to že roste jen [[cervena repa]] a možná [[kedlubny]]

## Záhon pod jabloní u kompostu:

 - [x] [[kveten]] 4: [[fazole]] žluté, [[cervena repa]]- Řepa vypadá že asi vzešla, ale asi byla nějak ožraná - nalezeny stonky pravděpodobně řepy, ale bez listů. Fazole asi nevzešly vůbec.
 - [x] [[kveten]] 23. vysazeny pórky z Vráže

## Záhon u udírny
 - [x] [[kveten]] 1. - vyplel jsem kopřivy i s kořenama, udělal řádky, a zasadil [[brambory]] co nám zbyly ve sklepě. Taky jsem zasadil tři brambory do pytlů. Na kraj záhonů jsem vysadil [[cibule]] a ranné [[cesnek]]
  - Update [[kveten]] 23. Brambory pěkně jedou, [[cibule]] a [[cesnek]] taky

## Záhon u kůlny (ten u vrátek):

 - [x]  [[duben]]: 24. [[hrasek]], [[mrkev]], [[kaderavek]], [[kapusta]] (směrem k vratům kolny)
  - Update [[kveten]] 23. Vypadá to že roste [[mrkev]] a [[hrasek]]

## Záhon u kůlny pískoviště

- [x] [[duben]]: 24 [[kaderavek]], [[mrkev]], [[kapusta]] -pár ks. směrem k vratům kolny
- Update [[kveten]] 23. Vypadá to že roste [[kapusta]]
 - [x] [[kveten]] 20. Jana vysadila dvě rajčata ("rajče jako rajče") přímo na záhonek.
- [x] [[kveten]] 9. - Den vítězství - zasadil jsem dvoje [[rajcata]] do sudu, nalevo kečupový, napravo orkán. Na kečupový mi spadl strom, ale zachránil jsem ho.
- [ ] [[zima]]: [[porek]]

## Záhon u třešně

 - [x]  [[kveten]] 7. Vyseta [[mrkev]], [[porek]] - nic z toho nevzešlo
 - [x] [[kveten]] 23. Po neúspěchu se setbou vysazeny předpěstované pórky z Vráže


## Nový jahodový záhon

  - [x] [[duben]] 24. Jahody vysázeny s loňských oddenků, cca 15 ks. do fólie - co nejmenší dírky, kouká vždy pár lístečků a srdíčko je přímo pod dírkou. Pár dní potom jsem ještě prostrkával listy dírkama.
  - Update [[kveten]] 23. - jahody docela rostou, pár květů, musel jsem na několikrát doprostrkávat jahodové lístky a srdíčka dírkami, jinak 0 péče.