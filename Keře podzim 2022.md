Objednávka z https://www.netradicniovoce-eshop.cz/

Objednáno následující: 

Asimina triloba cv.  ́Prima 1216 ́ - muďoul trojlaločný - samosprašná 1 1049,00 Kč 1049,00 Kč
Asimina triloba cv.  ́Sibley ́ - muďoul trojlaločný - pozdní odrůda 2 900,00 Kč 1800,00 Kč
[Muďouly pěstování](https://zahradkarskaporadna.cz/clanek-29641-mudoul-exoticky-krasavec-s-krasnymi-kvety-chutnymi-plody) - cizosprašný, hodně závlahy na jaře a na podzim, chránit před větrem, polostín, sklizeň po cca 5 letech září-říjen

[Višeň zakrslá kanadská  ́Carmine Jewel ́](https://www.netradicniovoce-eshop.cz/products/visen-zakrsla-%c2%b4carmine-jewel%c2%b4-kerik-%28mlada-rostlinka%29/) 1 250,00 Kč 250,00 Kč - nejrannější, cca 2m výšky

Ostružiník beztrnný  ́Thornless Evegreen ́ stříhanolistý 1 179,00 Kč 179,00 Kč
Ostružiník beztrnný  ́Black Satin ́ 1 179,00 Kč 179,00 Kč

Aronia  ́Viking ́ keřík 1 229,00 Kč 229,00 Kč
Aronia  ́Professor Ed ́ - keřík 1 165,00 Kč 165,00 Kč

[Morus sp.  ́Full Season ́](https://www.netradicniovoce-eshop.cz/products/moruse-%c2%b4full-season%c2%b4/) větší rostliny 1 449,00 Kč 449,00 Kč Extrémně plodná odrůda moruše. Dorůstá až do výšky 4,5 metru. Tmavé, chutné, tmavě fialové ovoce dozrává v červnu. Měli bychom ji zasadit na slunných místech chráněných před větrem. Je to rostlina odolná vůči suchu a nenáročná na půdní podmínky. Roste nejlépe na písčitých, lehkých a dobře odvodněných půdách s mírně kyselou a neutrální reakcí. Rostlina je odolná vůči mrazu, ale mladé exempláře je dobré chránit po dobu prvních 2-3 let v zimě a zabalit je netkanou textilií nebo slaměnou rohoží. Vypadá skvěle jako solitéra v trávníku nebo v kompozicích s jinými rostlinami.  

[Moruše  ́Illinois Everbearing ́](https://www.netradicniovoce-eshop.cz/products/morus-%c2%b4illinois-everbearing%c2%b4/) - větší rostliny 1 449,00 Kč 449,00 Kč -   spolehlivá odrůda velkoplodé moruše; jedná se o křížence Morus alba a Morus rubra. Odrůda je silně rostoucí, samosprašná. Plody jsou velké, 2-3cm dlouhé, v plné zralosti mají černou barvu. Chuťově sladké a aromatické. Dozrávají na přelomu července a srpna. Do plodnosti nastupuje velmi brzo, často již druhým rokem od výsadby. vytváří větší keř až menší strom do výšky 5m s řezem i menší.

[Morus alba  ́Milanówek ́](https://www.netradicniovoce-eshop.cz/products/morus-alba-milanowek/) 1 449,00 Kč 449,00 Kč - Silně rostoucí strom dosahuje výšky přibližně 8 m. Tato odrůda je velmi úrodná. Plody jsou středně velké, podlouhlé, sladké a aromatické. Plodí postupně po dlouhou dobu - od první poloviny června do konce září. Největší úrada je na začátku července. Půdy hlubší, živné dobře zásobené vodou. Ideální je slunné stanoviště.
  

[Elaeagnus umbellata - Hlošina okoličnatá (větší rostliny)](https://www.netradicniovoce-eshop.cz/products/hlosina-okolicnata-elaeagnus-umbellata-jaro-2017/) 1 285,00 Kč 285,00 Kč - chudší půda, Keř hlošiny okoličnaté dorůstá do výšky až 4 m a podobně do šířky, často však zůstává menší a celkem dobře odolává mrazu. Některou extrémní zimu se stane, že jeden z několika keřů částečně nebo úplně nad zemí omrzne, ale opět obrazí od paty kmínku. Tento druh je často šlechtěn a existují komerční kultivary nejen s většími plody, ale i jiným typem růstu.  

Josta keřová  ́Jostabes ́ - nová odrůda 1 169,00 Kč 169,00 Kč

Rybíz černý stromkový  ́Titania ́ - květináč 1 229,00 Kč 229,00 Kč
Rybíz červený keřový  ́Jonkheer van Tets ́ 1 149,00 Kč 149,00 Kč
Rybíz červený keřový  ́Rosetta ́ 1 159,00 Kč 159,00 Kč
Rybíz růžový keřový  ́Gloire de Sablon ́ NOVINKA!!! 1 199,00 Kč 199,00 Kč
Rybíz bílý stromkový  ́Blanka ́ - květináč 1 185,00 Kč 185,00 Kč

[Amelanchier alnifolia  ́Martin ́](https://www.netradicniovoce-eshop.cz/products/muchovnik-olsolisty-%c2%b4martin%c2%b4-roubovany-na-kminek-50-90cm/) 4 245,00 Kč 980,00 Kč Muchovník olšolistý ´Martin´ (Amelanchier alnifolia) - kanadská odrůda muchovníku, bujnější růst jako u ostatních odrůd tohoto druhu. Dorůstá v dospělosti do 3-5 m. Lze udržet občasným řezem ve výšce asi 2,5 m.  

Vinná réva  ́Glenora ́ - modrá bezsemenná 1 225,00 Kč 225,00 Kč
Vinná réva  ́Lakemont ́ - bílá bezsemenná 1 225,00 Kč 225,00 Kč
Vinná réva  ́ Teréz ́ - stolní odrůda 1 245,00 Kč 245,00 Kč
Vinná réva  ́New York Muscat ́ 1 245,00 Kč 245,00 Kč

[Surbopyrus auricularis  ́Tatarova ́](https://www.netradicniovoce-eshop.cz/products/hruskojerab-sorbopyrus-x-auricularis-%c2%b4tatarova%c2%b4-%28100-%29/) - hruškojeřáb 1 375,00 Kč 375,00 Kč -   Mezidruhový kříženec jeřábu a hrušně! Stará česká odrůda hruškojeřábu, která vznikla z náhodného německého semenáče "šípkové hrušně" a pravděpodobně jěřábu muku. Má malé asi 5 cm velké, kulaté, zlatavě žluté plody s netradiční kořenitou chutí. Dozrávají v první polovině září. Plody jsou vhodné pro přímý konzum i na kompoty. Není napadána rzí hrušňovou.
-   Ovocný druh vzácný pro svoji odolnost a schopnost růst i v horších klimatických podmínkach!!!
-   Výška: 160cm+